package com.padippurackal.cloud.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/hello")
public class HelloController {

    Logger logger = LoggerFactory.getLogger(HelloController.class);

    @GetMapping
    public String sayHello(@Value("${local.server.port}") String serverPort){
        logger.info("Received Hello request at Port " + serverPort);
        return "Hello from Eureka Client at Port " + serverPort;
    }
}
