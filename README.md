Service Registration and Discovery

This is a Spring Cloud Netflix Eureka Client.

This will allow client-side service discovery which will allow clients to find and communicate with each other. The only ‘fixed point’ in such an architecture consists of a service registry with which each service has to register.

A drawback is that all clients must implement a certain logic to interact with this fixed point. This assumes an additional network round trip before the actual request.

With Netflix Eureka each client can simultaneously act as a server, to replicate its status to a connected peer. In other words, a client retrieves a list of all connected peers of a service registry and makes all further requests to any other services through a load-balancing algorithm.

To be informed about the presence of a client, they have to send a heartbeat signal to the registry.

3 Microservices needs to be implemented.

1. a service registry (Eureka Server),
2. a REST service which registers itself at the registry (Eureka Client) and
3. a web-application, which is consuming the REST service as a registry-aware client (Spring Cloud Netflix Feign Client).
   and also uses a @LoadBalanced Spring RestTemplate to be able to route REST requests to the correct Eureka Client registered with the Eureka Server.
